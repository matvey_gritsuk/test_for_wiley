package API;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

class ActionClass {


    void getDelay(int delay) {
        given().contentType(ContentType.JSON)
                .log().all().when()
                .get("http://httpbin.org/delay/" + delay)
                .then().log().body().statusCode(200).extract().response();
    }

    void getImg(){
        given().contentType(ContentType.JSON)
                .log().all().when()
                .get("http://httpbin.org/image/png")
                .then().log().body().statusCode(200).extract().response();

    }


}