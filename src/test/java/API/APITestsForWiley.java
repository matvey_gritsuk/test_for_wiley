package API;

import org.testng.annotations.Test;

public class APITestsForWiley {

    private ActionClass actionClass = new ActionClass();

    /**
     * The test check that status of request is 200.
     */
    @Test
    public void checkGetDelay(){
        actionClass.getDelay(5);
    }

    /**
     * The test check that status of request is 200 and returns img.
     */
    @Test
    public void checkGetImg(){
        actionClass.getImg();
    }
}
