package GUI;

import GUI.pages.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class GUITestsForWiley extends PreparationForTests {

    private HomePage homePage;
    private MainPage mainPage;
    private StudentsPage studentsPage;
    private EducationPage educationPage;
    private GeneralClass generalClass;
    private ResultPage resultPage;

    @BeforeTest
    public void initPageObjects() throws IOException {
        homePage = PageFactory.initElements(driver, HomePage.class);
        mainPage = PageFactory.initElements(driver, MainPage.class);
        studentsPage = PageFactory.initElements(driver, StudentsPage.class);
        educationPage = PageFactory.initElements(driver, EducationPage.class);
        generalClass = PageFactory.initElements(driver, GeneralClass.class);
        resultPage = PageFactory.initElements(driver, ResultPage.class);

        homePage.open();
        mainPage.getURLFromHomePage();
    }

    //number 1
    @Test
    public void checkThatResourcesAsAnElementIsClickable() {
        mainPage.checkThatResourcesAsAnElementIsClickable();
    }

    //Task number 1
    @Test
    public void checkThatSubjectsAsAnElementIsClickable() {
        mainPage.checkThatSubjectsAsAnElementIsClickable();
    }

    //Task number 1
    @Test
    public void checkThatAboutAsAnElementIsClickable() throws InterruptedException {
        mainPage.checkThatAboutAsAnElementIsClickable();
    }

    //Task number 2
    /**
     * The test checks that items of Resources are correct. This test takes into account the fact that the names of
     * items are arranged on the page in alphabetical order. This test will not be executed because the documentation
     * states that the category "Resources" contains 10 elements. In fact, the category "Resources" on site
     * "https://www.wiley.com/en-us" contains 11 elements.
     */
    @Test
    public void checkThatItemsOfResourcesAreCorrect() {
        mainPage.clickResources();
        mainPage.checkThatItemsOfResourcesAreCorrect();
    }

    //Task number 3
    @Test
    public void checkThatStudentsHeaderIsDisplayed() throws InterruptedException {
        mainPage.clickResources();
        mainPage.clickElementStudents();
        studentsPage.checkThatHeaderOfTheStudentPageIsDisplayed();
    }

    //Task number 3
    /**
     * This test checks that "WileyPLUS" link exists on page and it leads to http://wileyplus.wiley.com/ URL.
     * This test will be failed because:
     * Expected :http://wileyplus.wiley.com/;
     * Actual   :https://www.wileyplus.com/;
     */
    @Test
    public void checkThatWileyPLUSLinkExistsOnPageAndItLeadsToSpecificURL() throws InterruptedException {
        mainPage.clickResources();
        mainPage.clickElementStudents();
        studentsPage.clickWileyPLUS();
        generalClass.checkThatACertainURLIsOpened("http://wileyplus.wiley.com/");
    }

    //Task number 4
    @Test
    public void checkEducationHeaderIsDisplayed() {
        mainPage.moveToSubjects();
        mainPage.moveToEL();
        mainPage.clickEducation();
        educationPage.checkThatEducationHeaderIsDisplayed();
    }

    //Task number 4
    @Test
    public void checkItemsAreDisplayedOnTheEducationPage() {
        mainPage.moveToSubjects();
        mainPage.moveToEL();
        mainPage.clickEducation();
        educationPage.checkThatAllEducationItemsAreDisplayedOnThePage(13);
    }

    //Task number 5
    @Test
    public void checkThatWhenClickOnTheLogoOpensTheHomePage() throws InterruptedException {
        mainPage.clickLogo();
        mainPage.checkTheHomePageIsOpened();
    }

    //Task number 6
    @Test
    public void checkThatIfPerformAnEmptySearchNothingWillHappen() {
        mainPage.clickSearchButton();
        mainPage.checkTheHomePageIsOpened();
    }

    //Task number 7
    @Test
    public void checkThatAreaWithRelatedContentIsDisplayed() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.checkThatAreaWithRelatedContentIsDisplayed();
    }

    //Task number 7
    @Test
    public void checkIfInputACertainWordToSearchInputWeWillSeeListOfWordsUnderTheSearchInputOnTheLeftSide() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.checkThatASpecifiedAmountOfResultsStartWithTheWordSpecifiedIntTheParameter("Math", 4);
    }

    //Task number 7
    @Test
    public void checkIfInputACertainWordToSearchInputWeWillSeeTitlesContainingThisWordUnderRelatedContentOnTheRightSide() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.checkASpecifiedAmountOfResultsOfRelatedContentContainTheWordOfParameter("Math", 4);
    }

    //Task number 8
    /**
     * This test will be failed because not all titles contain word "Math".
     * <p>
     * For example:
     * 'Understanding Power Quality Problems: Voltage Sags and Interruptions' doesn't contain 'Math'.
     * 'Integration of Distributed Generation in the Power System' doesn't contain 'Math'.
     * 'Signal Processing of Power Quality Disturbances' doesn't contain 'Math'.
     * <p>
     * But these blocks contain "Math" in the names of the authors. I decided that the titles are only text with link in
     * the block of each product.
     */
    @Test
    public void checkThatOnlyTitlesContainingWordOfParameterAreDisplayed() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.clickSearchButton();
        resultPage.checkAllTitlesOfResultPageContainTheWordOfParameter("Math");
    }

    //Task number 8
    @Test
    public void checkThatResultPageHasTenTitles() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.clickSearchButton();
        resultPage.checkThatResultPageHasTheNumberOfHeadersSpecifiedInTheParameter(10);
    }

    //Task number 8
    @Test
    public void checkThatEachTitleHasAtLeastOneButtonAddToCart() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.clickSearchButton();
        resultPage.checkThatResultPageHasTheNumberOfButtonsSpecifiedInTheParameter(10);
        resultPage.convertListOfResultsAndSaveIt();
    }

    //Task number 9
    @Test(dependsOnMethods = {"checkThatEachTitleHasAtLeastOneButtonAddToCart"})
    public void checkThatAfterReenteringTheWordSpecifiedInTheParametersTheSameTitlesAreDisplayed() {
        mainPage.inputACertainWordToSearchInput("Math");
        mainPage.clickSearchButton();
        resultPage.checkThatSavedListOfResultTitlesAndNewListOfResultTitlesAreEqual();
    }

    @AfterMethod
    public void goToMainPage() throws InterruptedException {
        mainPage.clickLogo();
    }
}
