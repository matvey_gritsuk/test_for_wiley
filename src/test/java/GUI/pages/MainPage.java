package GUI.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class MainPage extends Page {

    private static final Logger log = Logger.getLogger(String.valueOf(MainPage.class));

    private GeneralClass generalClass = new GeneralClass(driver);
    private Actions actions = new Actions(driver);

    private String homePage;

    @FindBy(xpath = "//div[@class = 'main-navigation-menu']/ul/li[1]/a[text()='Resources']")
    WebElement resources;

    @FindBy(xpath = "//div[@class = 'main-navigation-menu']/ul/li[2]/a[text()='Subjects']")
    WebElement subjects;

    @FindBy(xpath = "//div[@class = 'main-navigation-menu']/ul/li[3]/a[text()='About']")
    WebElement about;

    @FindBy(xpath = "//li[1]/div/div[@class = 'dropdown-items-wrapper']/h3")
    List<WebElement> listOfResources;

    @FindBy(xpath = "//h3[@class = 'third-level-menu']/a[text() = 'E-L']")
    WebElement EL;

    @FindBy(xpath = "//ul[@class='dropdown-items']/li/a[@title='Education']")
    WebElement education;

    @FindBy(xpath = "//div[@class='main-navigation-menu']/div[1]")
    WebElement logo;

    @FindBy(xpath = "//div/input[@type='search']")
    WebElement searchInput;

    @FindBy(xpath = "//div/span/button[@type='submit']")
    WebElement searchButton;

    @FindBy(xpath = "//form[@name='search_form_SearchBox']/div[2]")
    WebElement areaWithRelatedContent;

    @FindBy(xpath = "//div[@class = 'search-list']/div/a")
    List<WebElement> searchList;

    @FindBy(xpath = "//div/section/div[@class = 'related-content-products']/section/div[2]/h3/a")
    List<WebElement> listOfRelatedContent;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    /**
     * The method check that Element "Resources" is Displayed
     */
    public void checkThatResourcesAsAnElementIsDisplayed() {
        Assert.assertTrue(generalClass.isElementPresent(resources));
    }

    /**
     * The method check that Element "Resources" is clickable
     */
    public void checkThatResourcesAsAnElementIsClickable() {
        Assert.assertTrue(generalClass.isElementClickable(resources));
    }

    /**
     * The method check that Element "Subjects" is Displayed
     */
    public void checkThatSubjectsAsAnElementIsDisplayed() {
        Assert.assertTrue(generalClass.isElementPresent(subjects));
    }

    /**
     * The method check that Element "Subjects" is clickable
     */
    public void checkThatSubjectsAsAnElementIsClickable() {
        Assert.assertTrue(generalClass.isElementClickable(subjects));
    }

    /**
     * The method check that Element "About" is Displayed
     */
    public void checkThatAboutAsAnElementIsDisplayed() {
        Assert.assertTrue(generalClass.isElementPresent(about));
    }

    /**
     * The method check that Element "About" is clickable
     */
    public void checkThatAboutAsAnElementIsClickable() {
        Assert.assertTrue(generalClass.isElementClickable(about));
    }

    /**
     * The method create a list for checking Resources.
     *
     * @return list of Items from resources.
     */
    private ArrayList<String> createAListForResources() {
        ArrayList<String> resources = new ArrayList<>();
        resources.add("authors");
        resources.add("corporations");
        resources.add("institutions");
        resources.add("instructors");
        resources.add("librarians");
        resources.add("professionals");
        resources.add("researchers");
        resources.add("resellers");
        resources.add("societies");
        resources.add("students");

        return resources;
    }

    /**
     * The method check that items of Resources are correct. This method takes into account the fact that the names
     * of elements are arranged on the page in alphabetical order.
     */
    public void checkThatItemsOfResourcesAreCorrect() {
        Assert.assertEquals(generalClass.convertListOfWebElementsToString(listOfResources), createAListForResources());
    }

    /**
     * The method click Resources.
     */
    public void clickResources() {
        try {
            resources.click();
        } catch (NoSuchElementException e) {
            log.info("Element is not found!!!");
        }
    }

    /**
     * The method click on the category "Students"
     */
    public void clickElementStudents() {
        try {
            for (WebElement l : listOfResources) {
                if (l.getText().toLowerCase().equals("students")) {
                    l.click();
                    break;
                }
            }
        } catch (WebDriverException e) {
            log.info("Element isn't found or element isn't clickable!!!");
        }
    }

    /**
     * The method move to subjects.
     */
    public void moveToSubjects() {
        try {
            actions.moveToElement(subjects).build().perform();
        } catch (NoSuchElementException e) {
            log.info("Element is not found!!!");
        }
    }

    /**
     * The method move to E-L.
     */
    public void moveToEL() {
        try {
            actions.moveToElement(EL).build().perform();
        } catch (NoSuchElementException e) {
            log.info("Element is not found!!!");
        }
    }

    /**
     * The method click Education.
     */
    public void clickEducation() {
        try {
            education.click();
        } catch (NoSuchElementException e) {
            log.info("Element is not found!!!");
        }
    }

    /**
     * The method click logo.
     */
    public void clickLogo() throws InterruptedException {
        try {
            logo.click();
            Thread.sleep(300);
        } catch (NoSuchElementException e) {
            log.info("Element 'logo' is not found!!!");
        } catch (ElementNotVisibleException e) {
            log.info("Element 'logo' is not visible!!!");
        }
    }

    /**
     * The method click search button.
     */
    public void clickSearchButton(){
        try {
            searchButton.click();
        } catch (NoSuchElementException e) {
            log.info("Element 'searchButton' is not found!!!");
        } catch (ElementNotVisibleException e) {
            log.info("Element 'searchButton' is not visible!!!");
        }
    }

    /**
     * The method assigns the URL of home page to the element "homePage".
     */
    public void getURLFromHomePage(){
        homePage = driver.getCurrentUrl();
    }

    /**
     * The method checks that home page is opened.
     */
    public void checkTheHomePageIsOpened(){
        Assert.assertEquals(driver.getCurrentUrl(), homePage);
    }

    /**
     * The method inputs the certain word to search input
     */
    public void inputACertainWordToSearchInput(String word){
        try {
            searchInput.clear();
            searchInput.sendKeys(word);
        } catch (NoSuchElementException e) {
            log.info("Element 'searchButton' is not found!!!");
        } catch (ElementNotVisibleException e) {
            log.info("Element 'searchButton' is not visible!!!");
        }
    }

    /**
     * The method checks that area with Related Content are displayed.
     */
    public void checkThatAreaWithRelatedContentIsDisplayed(){
        Assert.assertTrue(generalClass.isElementPresent(areaWithRelatedContent));
    }

    /**
     * The method check if all words start with the word of parameter of this method.
     *
     * @param word inputted in search
     * @param amountOfWords - amount of words of results
     * @return true if all words start with words of parameter
     *         false if at least one word doesn't contain
     */
    private boolean checkIfResultsOfSearchStartWithWordSpecifiedInTheParameter(String word, int amountOfWords){
        int i = 0;
        for(WebElement l:searchList){
            if(l.getText().startsWith(word)){
                i++;
            }else{
                log.info("Element with name '" + l.getText() + "' doesn't start with '" + word + "'");
            }
        }
        return i == amountOfWords;
    }

    /**
     * The method checks that a specified amount of results start with the word specified in the parameter.
     *
     * @param word inputted in search
     * @param amountOfWords - amount of words of results
     */
    public void checkThatASpecifiedAmountOfResultsStartWithTheWordSpecifiedIntTheParameter(String word, int amountOfWords){
        Assert.assertTrue(checkIfResultsOfSearchStartWithWordSpecifiedInTheParameter(word, amountOfWords));
    }

    /**
     * The method check that a certain number of related content contain the word of parameter of this method
     *
     * @param word inputted in search
     * @param amountOfResult - amount of strings of results
     * @return true if all strings contain with words of parameter
     *         false if at least one string doesn't contain
     */
    private boolean checkACertainNumberOfStringsOfRelatedContentContainTheWordOfParameter(String word, int amountOfResult){
        int i = 0;
        for(WebElement l:listOfRelatedContent){
            if(l.getText().contains(word)){
                i++;
            }else{
                log.info("String '" + l.getText() + "' doesn't contain '" + word + "'");
            }
        }
        return i == amountOfResult;
    }

    /**
     * The method checks that a specified amount of results contain the word specified in the parameter.
     *
     * @param word inputted in search
     * @param amountOfResult - amount of strings of results
     */
    public void checkASpecifiedAmountOfResultsOfRelatedContentContainTheWordOfParameter(String word, int amountOfResult){
        Assert.assertTrue(checkACertainNumberOfStringsOfRelatedContentContainTheWordOfParameter(word, amountOfResult));
    }


}
