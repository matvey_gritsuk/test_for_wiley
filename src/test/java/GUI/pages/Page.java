package GUI.pages;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public abstract class Page {

    WebDriver driver;

    Page(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

}
