package GUI.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class StudentsPage extends Page {

    private GeneralClass generalClass = new GeneralClass(driver);
    private static final Logger log = Logger.getLogger(String.valueOf(StudentsPage.class));

    @FindBy(xpath = "//div[@class = 'content cke-content']/p[text() = 'Students']")
    WebElement studentHeader;

    @FindBy(xpath = "//a[text() = 'WileyPLUS']")
    WebElement wileyPLUS;

    public StudentsPage(WebDriver driver) {
        super(driver);
    }

    /**
     * The method check that header "Student" is displayed.
     */
    public void checkThatHeaderOfTheStudentPageIsDisplayed() {
        Assert.assertTrue(generalClass.isElementPresent(studentHeader));
    }

    /**
     * The method clicks on link "WileyPLUS"
     */
    public void clickWileyPLUS() throws InterruptedException {
        try {
            wileyPLUS.click();
            Thread.sleep(300);
        } catch (NoSuchElementException e) {
            log.info("Element is not found!!!");
        }
    }


}
