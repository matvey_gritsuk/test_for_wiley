package GUI.pages;

import GUI.util.PropertyLoader;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class HomePage extends Page {

    private static String baseURL;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    /**
     * Gets the url from pom.xml.
     * Opens webpage with this url.
     */
    public void open() throws IOException {
        baseURL = PropertyLoader.loadProperty("site.url");
        driver.get(baseURL);
    }

}
