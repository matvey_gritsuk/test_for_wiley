package GUI.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class EducationPage extends Page {

    private GeneralClass generalClass = new GeneralClass(driver);
    private static final Logger log = Logger.getLogger(String.valueOf(EducationPage.class));

    @FindBy(xpath = "//div[@class = 'wiley-slogan']/h1[3]/span[text()='Education']")
    WebElement headerEducation;

    @FindBy(xpath = "//div[@class = 'side-panel']/ul/li")
    List<WebElement> listOfEducationItems;

    public EducationPage(WebDriver driver) {
        super(driver);
    }

    /**
     * The method check that header "Education" is displayed.
     */
    public void checkThatEducationHeaderIsDisplayed() {
        Assert.assertTrue(generalClass.isElementPresent(headerEducation));
    }

    /**
     * The method create education items for checking real items on the page.
     *
     * @return list with items from education page.
     */
    public ArrayList<String> createListWithEducationItemsForCheckingRealEducationItemsOnPage() {
        ArrayList<String> educationItems = new ArrayList<>();
        educationItems.add("Information & Library Science");
        educationItems.add("Education & Public Policy");
        educationItems.add("K-12 General");
        educationItems.add("Higher Education General");
        educationItems.add("Vocational Technology");
        educationItems.add("Conflict Resolution & Mediation (School settings)");
        educationItems.add("Curriculum Tools- General");
        educationItems.add("Special Educational Needs");
        educationItems.add("Theory of Education");
        educationItems.add("Education Special Topics");
        educationItems.add("Educational Research & Statistics");
        educationItems.add("Literacy & Reading");
        educationItems.add("Classroom Management");

        return educationItems;
    }

    /**
     * The method check what a certain amount of education items exist on the page.
     *
     * @param realCountOfItems - the number of items that should be
     * @return true if all elements exist
     *         false if
     */
    private boolean checkWhatACertainAmountOfEducationItemsExistOnThePage(int realCountOfItems) {
        ArrayList<String> namesOfItems = createListWithEducationItemsForCheckingRealEducationItemsOnPage();
        int i = 0;
        for (WebElement l : listOfEducationItems) {
            if (namesOfItems.contains(l.getText())) {
                i++;
            }else{
                log.info("Element with name '" + l.getText() + "' doesn't exist");
            }
        }
        return i == realCountOfItems;
    }

    /**
     * The method check that all education items are displayed on the page.
     */
    public void checkThatAllEducationItemsAreDisplayedOnThePage(int realCountOfItems){
        Assert.assertTrue(checkWhatACertainAmountOfEducationItemsExistOnThePage(realCountOfItems));
    }
}
