package GUI.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class GeneralClass extends Page {

    private static final Logger log = Logger.getLogger(String.valueOf(GeneralClass.class));

    public GeneralClass(WebDriver driver) {
        super(driver);
    }

    /**
     * The method determines the existence of the web element.
     *
     * @return true if element was found
     *         false if element wasn't found
     */
    boolean isElementPresent(WebElement webElement){
        try{
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(webElement));
            return true;
        }catch (NoSuchElementException e){
            log.info("Element with name" + webElement.getText() + "isn't found!!!");
            return false;
        }
    }

    /**
     * The method determines the clickability of the web element.
     * @return true if element is clickable
     *         false if element isn't clickable
     */
    boolean isElementClickable(WebElement webElement){
        try{
            webElement.click();
            return true;
        }catch (WebDriverException e){
            log.info("Element isn't clickable!!!");
            return false;
        }
    }

    /**
     * The method check that correct URL is opened.
     */
    public void checkThatACertainURLIsOpened(String expectedURL){
        if(driver.getCurrentUrl().equals(expectedURL)){
            driver.navigate().back();
            Assert.assertTrue(true);
        }else {
            driver.navigate().back();
            Assert.assertTrue(false);
        }
    }


    /**
     * The method convert list of WebElements to list of Strings
     *
     * @return listOfConvertedWebElements
     */
    ArrayList<String> convertListOfWebElementsToString(List<WebElement> webList) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (WebElement l : webList) {
            arrayList.add(l.getText().toLowerCase());
        }
        return arrayList;
    }



}
