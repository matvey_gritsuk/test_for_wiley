package GUI.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class ResultPage extends Page {

    private ArrayList<String> savedListOfResultTitle;

    @FindBy(xpath = "//h3[@class = 'product-title']/a")
    List<WebElement> listOfResultTitle;

    @FindBy(xpath = "//div[@class='product-table-body']/div[1]/div/div[last()]/div/form/button[text()='Add to cart']")
    List<WebElement> listOfButton;

    private static final Logger log = Logger.getLogger(String.valueOf(MainPage.class));
    private GeneralClass generalClass = new GeneralClass(driver);

    public ResultPage(WebDriver driver) {
        super(driver);
    }

    /**
     * The method checks if all titles contain the word from the method parameter and returns true if all titles
     * contain this word
     *
     * @param word inputted in search
     * @return true if all strings contain with words of parameter
     * false if at least one string doesn't contain
     */
    private boolean checkIfAllTitlesContainTheWordFromTheMethodParameter(String word) {
        try {
            int i = 0;
            for (WebElement l : listOfResultTitle) {
                if (l.getText().contains(word)) {
                    i++;
                } else {
                    log.info("String '" + l.getText() + "' doesn't contain '" + word + "'");
                }
            }
            return i == listOfResultTitle.size();
        }catch (NoSuchElementException e){
            log.info("Element isn't found!!!");
            return false;
        }
    }

    /**
     * The method checks that all titles of result page contain the word from parameter of this method.
     *
     * @param word inputted in search
     */
    public void checkAllTitlesOfResultPageContainTheWordOfParameter(String word) {
        Assert.assertTrue(checkIfAllTitlesContainTheWordFromTheMethodParameter(word));
    }

    /**
     * The method check that result page has the number of titles specified in the parameter.
     *
     * @param amountOfResultTitles - amount of titles that should be on result page
     */
    public void checkThatResultPageHasTheNumberOfHeadersSpecifiedInTheParameter(int amountOfResultTitles){
        Assert.assertEquals(amountOfResultTitles, listOfResultTitle.size());
    }

    /**
     * The method check that result page has the number of buttons specified in the parameter of this method.
     *
     * @param amountOfButton - amount of button that should be on result page
     */
    public void checkThatResultPageHasTheNumberOfButtonsSpecifiedInTheParameter(int amountOfButton){
        Assert.assertEquals(amountOfButton, listOfButton.size());
    }

    /**
     * The method convert list of result titles and save it.
     */
    public void convertListOfResultsAndSaveIt(){
        savedListOfResultTitle = generalClass.convertListOfWebElementsToString(listOfResultTitle);
    }

    /**
     * The method check that saved list of result titles and new list of result titles are equal.
     */
    public void checkThatSavedListOfResultTitlesAndNewListOfResultTitlesAreEqual(){
        Assert.assertEquals(savedListOfResultTitle, generalClass.convertListOfWebElementsToString(listOfResultTitle));
    }
}
